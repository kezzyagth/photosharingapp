import {
    GET_FEED_STARTED,
    GET_FEED_SUCCESS,
    GET_FEED_FAILURE,
    POST_USER_LIST_STARTED,
    POST_USER_LIST_SUCCESS,
    POST_USER_LIST_FAILURE,
    GET_USER_LIST_STARTED,
    GET_USER_LIST_SUCCESS,
    GET_USER_LIST_FAILURE,
    LOGIN_STARTED,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    UPDATE_USER_LIST_STARTED,
    UPDATE_USER_LIST_SUCCESS,
    UPDATE_USER_LIST_FAILURE,
    UPLOAD_IMAGE_STARTED,
    UPLOAD_IMAGE_SUCCESS,
    UPLOAD_IMAGE_FAILURE,
    POST_FEED_STARTED,
    POST_FEED_SUCCESS,
    POST_FEED_FAILURE
} from '../action/types';

const initialState={
    loading: false,
    users:[],
    error: null,
    feeds:[]
}

const userReducer=(state=initialState, action) => {
    switch (action.type){
            case GET_FEED_STARTED:
                return{
                    ...state,
                    loading: true
                }
            case GET_FEED_SUCCESS:
                return {
                    ...state,
                    error: null,
                    loading: false,
                    feeds: action.payload
                }
            case GET_FEED_FAILURE:
                return {
                    ...state,
                    loading: false,
                    error: action.payload.error
            }
            case UPDATE_USER_LIST_STARTED:
                return{
                    ...state,
                    loading: true
            }
            case UPDATE_USER_LIST_SUCCESS:
                return{
                    ...state,
                    error: null,
                    loading: false,
                    users: action.payload
            }
            case UPDATE_USER_LIST_FAILURE:
                return{
                    ...state,
                    loading: false,
                    error: action.payload.error
            }
            case LOGIN_STARTED:
                return{
                    ...state,
                    loading: true
            }
            case LOGIN_SUCCESS:
                return{
                    ...state,
                    error: null,
                    loading: false,
                    users: action.payload
            }
            case LOGIN_FAILURE:
                return{
                    ...state,
                    loading: false,
                    error: action.payload.error
            }
            case UPLOAD_IMAGE_STARTED:
                return{
                    ...state,
                    loading: true
            }
            case UPLOAD_IMAGE_SUCCESS:
                return{
                    ...state,
                    error: null,
                    loading: false,
                    users: action.payload
            }
            case UPLOAD_IMAGE_FAILURE:
                return{
                    ...state,
                    loading: false,
                    error: action.payload.error
            }
            case GET_USER_LIST_STARTED:
                return{
                    ...state,
                    loading: true
            }
            case GET_USER_LIST_SUCCESS:
                return{
                    ...state,
                    error: null,
                    loading: false,
                    users: action.payload
            }
            case GET_USER_LIST_FAILURE:
                return{
                    ...state,
                    loading: false,
                    error: action.payload.error
            }
            case POST_FEED_STARTED:
                return{
                    ...state,
                    loading: true
            }
            case POST_FEED_SUCCESS:
                return{
                    ...state,
                    error: null,
                    loading: false,
                    users: action.payload
            }
            case POST_FEED_FAILURE:
                return{
                    ...state,
                    loading: false,
                    error: action.payload.error
            }
                default :
                 return state
    }
}

export {userReducer}