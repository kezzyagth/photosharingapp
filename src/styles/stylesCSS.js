import {StyleSheet} from 'react-native';

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#b3e7ff',
      },
      list: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        borderWidth: 3,
        borderColor: 'black',
        backgroundColor: '#b3e7ff',
      },
      caption:{
        fontSize: 15,
        marginBottom:20,
        fontWeight: 'bold'
      },
      images:{
        width:350, 
        height:300,
        borderColor: 'black',
        borderWidth:4,
        marginBottom: 10
      },
      inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius:30,
        borderBottomWidth: 1,
        width:250,
        height:45,
        marginBottom:20,
        flexDirection: 'row',
        alignItems:'center'
    },
    textInput: {
        alignItems: 'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    inputIcon:{
        width:30,
        height:30,
        marginLeft:15,
        justifyContent: 'center'
      },
    buttonContainer: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
    },
    loginButton: {
      backgroundColor: "#00b5ec",
    },
    loginText: {
      color: 'white',
    },
    circleImageView: {
        width: 200, 
        height: 200, 
        borderRadius: 200 / 2,
        marginBottom: 30,
        borderStyle: 'solid',
        borderColor: 'black',
        borderWidth: 4,
        alignItems: 'center',
    },
    headerText: {
        alignItems: 'center',
        marginBottom: 30,
        fontSize: 40,
        color: 'white',
        fontWeight: 'bold'
    },
    errormessage: {
      color: 'red'
    },
    blankFeed: {
      fontSize: 20,
      textAlign: 'center',
      justifyContent: 'center',
      fontWeight: 'bold',
      color: 'gray'
    },
    circleImageView2: {
      width: 150, 
      height: 150, 
      borderRadius: 200 / 2,
      marginBottom: 30,
      borderStyle: 'solid',
      borderColor: 'black',
      borderWidth: 4,
      alignItems: 'center',
  },fab:{
    height: 50,
    width: 50,
    borderRadius: 200,
    position: 'absolute',
    bottom: 20,
    right: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#686cc3',
  },
  text:{
    fontSize:30,
    color:'white'
  },
  container2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
})

export default style;