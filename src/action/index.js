import {
    GET_FEED_STARTED,
    GET_FEED_SUCCESS,
    GET_FEED_FAILURE,
    POST_USER_LIST_STARTED,
    POST_USER_LIST_SUCCESS,
    POST_USER_LIST_FAILURE,
    LOGIN_STARTED,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    UPDATE_USER_LIST_STARTED,
    UPDATE_USER_LIST_SUCCESS,
    UPDATE_USER_LIST_FAILURE,
    UPLOAD_IMAGE_STARTED,
    UPLOAD_IMAGE_SUCCESS,
    UPLOAD_IMAGE_FAILURE,
    POST_FEED_STARTED,
    POST_FEED_FAILURE,
    POST_FEED_SUCCESS,
    GET_IMAGE_STARTED,
    GET_IMAGE_SUCCESS,
    GET_IMAGE_FAILURE
} from './types';
import axios from 'axios';

const getImageStarted = () => {
    return{
        type: GET_IMAGE_STARTED,       
    }
}

const getImageFailure = error => {
    return{
        type: GET_IMAGE_FAILURE,
        payload: error
    }
}

const getImageSuccess = users => {
    return{
        type: GET_IMAGE_SUCCESS,
        payload: users
    }
}

const getFeedStarted = () => {
    return{
        type: GET_FEED_STARTED,       
    }
}

const getFeedFailure = error => {
    return{
        type: GET_FEED_FAILURE,
        payload: error
    }
}

const getFeedSuccess = users => {
    return{
        type: GET_FEED_SUCCESS,
        payload: users
    }
}


const PostUserListStarted = () => {
    return{
        type: POST_USER_LIST_STARTED,       
    }
}

const PostUserListFailure = error => {
    return{
        type: POST_USER_LIST_FAILURE,
        payload: error
    }
}

const PostUserListSuccess = users => {
    return{
        type: POST_USER_LIST_SUCCESS,
        payload: users
    }
}

const updateUserListStarted = () => {
    return{
        type: UPDATE_USER_LIST_STARTED,       
    }
}

const updateUserListFailure = error => {
    return{
        type: UPDATE_USER_LIST_FAILURE,
        payload: error
    }
}

const updateUserListSuccess = users => {
    return{
        type: UPDATE_USER_LIST_SUCCESS,
        payload: users
    }
}

const loginStarted=()=>{
    return {
        type:LOGIN_STARTED
    }
}

const loginSuccess=(userDetail)=>{
    return {
        type:LOGIN_SUCCESS,
        payload:userDetail
    }
}

const loginFailure=(error)=>{
    return {
        type:LOGIN_FAILURE,
        payload:error
    }
}

const uploadImageStarted = () => {
    return{
        type: UPLOAD_IMAGE_STARTED,       
    }
}

const uploadImageFailure = error => {
    return{
        type: UPLOAD_IMAGE_FAILURE,
        payload: error
    }
}

const uploadImageSuccess = users => {
    return{
        type: UPLOAD_IMAGE_SUCCESS,
        payload: users
    }
}

const postFeedStarted = () => {
    return{
        type: POST_FEED_STARTED,       
    }
}

const postFeedFailure = error => {
    return{
        type: POST_FEED_FAILURE,
        payload: error
    }
}

const postFeedSuccess = users => {
    return{
        type: POST_FEED_SUCCESS,
        payload: users
    }
}

export const getFeed = (email) => {
    return dispatch => {
        dispatch(getFeedStarted())
        axios.get(`http://20.4.12.213:3000/user/feed/${email}`, {
        })
        .then(res => {
            dispatch(getFeedSuccess(res.data))
        }).catch(err => {
            dispatch(getFeedFailure(err))
        })
    }
}

export const getImage = (image) => {
    return dispatch => {
        dispatch(getImageStarted())
        axios.get(`http://20.4.12.213:3000/images/${image}`, {
        })
        .then(res => {
            dispatch(getImageSuccess(res.data))
        }).catch(err => {
            dispatch(getImageFailure(err))
        })
    }
}

export const postUser = payload => {
    return dispatch => {
        dispatch(PostUserListStarted())
        return axios.post(`http://20.4.12.213:3000/user/create`, payload)
        .then(response => {
            dispatch(PostUserListSuccess(response.data));
            return response.data
        }).catch(err => {
            dispatch(PostUserListFailure(err));
            return err;
        })
    }
}

export const loginUser = payload => {
    return dispatch => {
        dispatch(loginStarted())
        return axios.post(`http://20.4.12.213:3000/user/login`, payload)
        .then(response => {
            dispatch(loginSuccess(response.data));
            return response.data
        }).catch(err => {
            dispatch(loginFailure(err));
            return err;
        })
    }
}

export const updateUser = (email,payload) => {
    return dispatch => {
        dispatch(updateUserListStarted())
        return axios.put(`http://20.4.12.213:3000/user/${email}`, payload)
        .then(response => {
            dispatch(updateUserListSuccess(response.data));
            return response.data
        }).catch(err => {
            dispatch(updateUserListFailure(err));
            return err;
        })

    }

}


export const postFeed = (email, payload) => {
    
    return dispatch => {
        dispatch(postFeedStarted())
        return axios.post(`http://20.4.12.213:3000/user/image/${email}`, payload)
        .then(response => {
            dispatch(postFeedSuccess(response.data));
            return response.data
        }).catch(err => {
            dispatch(postFeedFailure(err));
            return err;
        })
    }
}
// export const postImage = (email,imageData, imageSource, payload) => {
//     // const data = new FormData();

//     // data.append('image',{
//     //     name: "Image.jpg",
//     //     type: "image/jpg",
//     //     uri: image.uri
//     // })

//     return dispatch => {
//         dispatch(postFeedStarted())
//         return axios.post(`http://20.4.12.213:3000/user/create/image/${email}`
//         ).then((response) => {
//             dispatch(postFeedSuccess(response.data));
//             return response.data
//         }).catch(err => {
//             dispatch(postFeedFailure(err));
//             return err;
//         })
//     }
// }    
        

