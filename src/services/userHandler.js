const {User,Image} = require('./user');
const fs = require('fs');

const notFoundResponse={
    statusCode:404,
    error:`Not Found`,
    message:`Not found`
}

const conflictResponse={
    statusCode:409,
    error:`Conflict`,
    message:`Conflict request input`
}

const getFeed = async (request,h) => {
    const getEmail = request.params.email
    const getPhoto = await Image.find({email:getEmail}).lean()
    return getPhoto
}

// const getPhoto = async (request, h) => {
//     return h.file(`${request.params.image}`).code(200)
// }

const getAllUser = function (request, h) {
    request.log('error', 'Event error');
    logging(request, h)
    return User.find()
}

const getAllImage = function (request, h) {
    request.log('error', 'Event error');
    logging(request, h)
    return Image.find()
}

const getUser = async (filter) => {
    return await PerformanceTiming.findOne({id:filter}).lean()
}


const postUser=async(request,h)=>{
    const payload=request.payload
    const users=await User.aggregate([
        {
            $group:{
                _id:`email`,
                emails:{
                    $push:`$email`
                }
            }
        }
    ])
    if(users[0]){
        const emailAvailability=users[0].emails.some(email=>email==payload.email)
        if(emailAvailability){
            return h.response(conflictResponse).code(409)
        }
    }
    await User.insertMany([payload])
    return h.response(payload).code(201)
}

const loginUser=async(request, h) => {
    const payload = request.payload
    const result = await User.findOne(payload).lean()
    if(!result){
        return h.response(notFoundResponse).code(404)
    }
    return h.response(result).code(200)
}


const updateUser=async(request,h)=>{
    const {payload,params}=request
    const users=await User.aggregate([
        {
            $group:{
                _id:'uniqueHandles',
                uniqueHandles:{
                    $push:'$uniqueHandle'
                }
            }
        }
    ])  
    time = Date.now()
    fileName = time + '.jpg'
    imageName = params.email+ fileName 
    fs.writeFile('./images/profile/' + imageName, payload.imageSource, 'base64', ()=>{});
    if(users[0]){
        const handleAvailability=users[0].uniqueHandles.some(handle=>handle==payload.uniqueHandle)
      
        if(handleAvailability){
            return h.response(conflictResponse).code(409)
        }
    }
    await Image.insertMany([{
        name:fileName,
        email: params.email,
        caption: "Update profile DP",
        dateTime: time,
        type: "DP"
    }])
    console.log(params.email)
    const updatedUser=await User.findOneAndUpdate({email:params.email},payload)
    if(!updatedUser){
        return h.response(notFoundResponse).code(404)
    }
    return h.response(updatedUser).code(202)
}

const postImage = async (request, h) => {
     const {payload, params} = request
    time = Date.now()
    fileName = time + '.jpg'
    imageName = params.email+ fileName 
    fs.writeFile('./images/feed/' + imageName, payload.imageData, 'base64', ()=>{});
   await Image.insertMany([{
    name:fileName,
    email: params.email,
    caption: payload.caption,
    dateTime: time,
    type: "FEED"
}])
   return h.response(payload).code(201)
}

const logging = async (request, h) =>{
    request.log(['a', 'b'], 'Request into User')
 
    request.logger.info('In handler %s', request.path)

    return 'Hello!'
}


module.exports = {getAllUser, getUser, postUser, updateUser,loginUser,getAllImage,postImage,getFeed/* ,getPhoto} */}