let mongoose = require('mongoose')
let Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost/photosharing', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set('debug', true);

const userSchema = new Schema(
    {
        id:Number,
        firstName:String,
        lastName:String,
        uniqueHandle:String,
        email:String,
        password:String,
        profilePic:String
    }
);

const imageSchema = new Schema(
    {
        id:Number,
        name:String,
        email: String,
        caption: String,
        likes: [String],
        dateTime: Number,
        type: String
    }
  
)

const User = mongoose.model('User', userSchema);
const Image = mongoose.model('Image', imageSchema)

module.exports = {User,Image,imageSchema,userSchema}
