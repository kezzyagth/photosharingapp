const Path = require('path');
const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi')
const Bcrypt = require('bcryptjs');
const Qs = require('qs');
const Inert = require('@hapi/inert');
const CatboxRedis = require('@hapi/catbox-redis');
const { getUser, getAllUser, postUser, updateUser, loginUser, getAllImage,postImage,getFeed ,/* getPhoto */} = require('./userHandler')



const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

const validate = async (request, username, password) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};
    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0',
        cache:[
                {
                    name: 'my_cache',
                    provider: {
                        constructor: CatboxRedis,
                        options: {
                            partition : 'my_cached_data',
                            host: 'localhost',
                            port: 6379,
                            database: 0
                        }
                    }
                }
            ],
        query: {
            parser: (query) => Qs.parse(query)
        }
});


exports.init = async () => {

    await server.initialize();
    return server;
};

exports.start = async () => {
    await server.register([require('@hapi/inert'),require('@hapi/basic')]);
    await server.register({
        plugin: require('hapi-pino'),
        options: {
          prettyPrint: process.env.NODE_ENV !== 'production',
          stream:'./logs.log',
          redact: ['req.headers.authorization']
        }
    })

    server.auth.strategy('simple', 'basic', { validate });

    server.route({
        method: 'GET',
        path: '/user',
        handler: getAllUser, 
        options: {
            auth: 'simple',
            log:{
                collect: true
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/image',
        handler: getAllImage, 
        options: {
            log:{
                collect: true
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/imagesFolder/{image*}',
        handler: {
            directory: {
                path: './images/feed',
                listing: true
            }
        }
    });

    server.route({
        method:'GET',
        path: '/user/feed/{email}',
        handler: getFeed
    })

    // server.route({
    //     method:'GET',
    //     path:'/images/{image}',
    //     handler:getPhoto
    // })

    server.route({
        method: "POST",
        path: "/user/create",
        options: {
           /*  validate: {
                payload: {
                    id:Joi.any().forbidden(),
                    email:Joi.string().required(),
                    password:Joi.string().min(8).required()
                }
            }, */
        },
        handler: postUser
    });

    server.route({
        method: "POST",
        path: "/user/login",
        handler: loginUser,
    })


    server.route({
        method: 'PUT',
        path: '/user/{email}',
        handler: updateUser,
    });

    server.route({
        method: 'POST',
        path: '/user/image/{email}',
        handler: postImage,
    })
    
    server.method('getUser',getUser,{
        cache: {
            cache: 'my_cache',
            segment: 'User',
            expiresIn: 1000 * 1000,
            generateTimeout: 2000
        }
    })

    server.logger().info('Logging.log')

    server.log(['subsystem'], 'third way for accessing it')

    await server.start();
    console.log('Server running at:', server.info.uri);
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
})
;