import React, {Component} from 'react';
import {Text,TextInput, View, Image, TouchableOpacity} from 'react-native';
import style from '../styles/stylesCSS';
import {validate} from 'validate.js';
import constraints from './constraints/constraints';
import { connect } from 'react-redux';
import {postUser} from '../action/index';

class register extends Component {
    static navigationOptions={
        header:null
    }
    
    constructor(){
        super()
        this.state={
            data: {
                email: '',
                password: '',
                cpassword: '',
            },
            ButtonStateHolder : true,
            errors: '',
        };
        this.validate = this.validate.bind(this);
        }
        EnableButtonFunction = () => {
            this.setState({
                ButtonStateHolder : false,
            });
        }
        DisableButtonFunction = () => {
            this.setState({
                ButtonStateHolder : true,
            });
        }

        validate() {
            const validationResult = validate(this.state.data, constraints);
            this.setState({ errors: validationResult });
            if (!this.state.errors){
                this.EnableButtonFunction();
            } else {
                this.DisableButtonFunction();
            }
        }

        RegisterNewUser(){
            this.props.postUser({
                email: this.state.data.email,
                password: this.state.data.password,
            }).then(response => {
                this.props.navigation.navigate('profile',response)
            })
        }

    render() {
        return (
            <View style={style.container}>
                <View>
                    <Text style={style.headerText}>Sign Up</Text>
                </View>
                
                <View style={style.inputContainer}>
                    <Image 
                        style={style.inputIcon}
                        source={require('../images/email.png')} />

                    <TextInput
                        textContentType="emailAddress"
                        placeholder="Type Your Email Here"
                        value={this.state.data.email}
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, email: text}})
                            }
                        value={this.state.data.email}
                        onEndEditing={()=>this.validate()}
                    />

                </View>
                <View style={style.inputContainer}>
                    <Image 
                        style={style.inputIcon}
                        source={require('../images/lock.png')} />
                    <TextInput
                        textContentType="password"
                        secureTextEntry={true}
                        placeholder="Type Your Password Here"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, password: text}})
                            }
                        onEndEditing={()=>this.validate()}

                    />
                </View>
                <View style={style.inputContainer}>
                <Image 
                        style={style.inputIcon}
                        source={require('../images/key.png')} />
                    <TextInput
                        textContentType="password"
                        secureTextEntry={true}
                        placeholder="Please Re-enter Your Password"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, cpassword: text}})
                        }
                        onEndEditing={()=>this.validate()}
                    />
                </View>
                 <View>
                    <Text style={style.errormessage}>{this.getErrorMessages()}</Text>
                    </View>
                <View>
                    <TouchableOpacity style={[style.buttonContainer, style.loginButton]}
                        disabled={this.state.ButtonStateHolder}
                        onPress={()=>this.RegisterNewUser()}>
                        <Text style={style.loginText}>Submit</Text>
                    </TouchableOpacity>
       
                    <TouchableOpacity style={style.buttonContainer} onPress={() => this.props.navigation.navigate('login')}>
                        <Text>back to login</Text>
                    </TouchableOpacity>
                   
                </View>
            </View>
        )
    }
getErrorMessages(separator = '\n') {
    const { errors } = this.state;
    if (!errors) {return [];}
    return Object.values(errors).map(it => it.join(separator)).join(separator);
  }
  getErrorsInField(field) {
    const { errors } = this.state;
    return errors && errors[field] || [];
  }
  isFieldInError(field) {
    const { errors } = this.state;
    return errors && !!errors[field];
  }


}


const mapStateToProps = state => {
    return{
        user: state.user
    }
}

export default connect(mapStateToProps,{postUser})(register)