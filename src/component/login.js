import React, {Component} from 'react';
import {Text, View, TextInput, TouchableHighlight, Image} from 'react-native';
import style from '../styles/stylesCSS';
import constraintsLogin from './constraints/constraintsLogin'
import {loginUser} from '../action/index';
import {validate} from 'validate.js';
import {connect} from 'react-redux'

class login extends Component {
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props)
        this.state={
        data:{
            email: '',
            password: '',
        },   
        errors: '',
        ButtonStateHolder : true
        };
        this.validate = this.validate.bind(this);
    }
    EnableButtonFunction = () => {
        this.setState({
            ButtonStateHolder : false,
        });
    }
    DisableButtonFunction = () => {
        this.setState({
            ButtonStateHolder : true,
        });
    }

    validate() {
        const validationResult = validate(this.state.data, constraintsLogin);
        this.setState({ errors: validationResult });
        if (!this.state.errors){
            this.EnableButtonFunction();
        } else {
            this.DisableButtonFunction();
        }
    }

    // UserLogin(){
    //     if(!this.state.errors){
    //         Axios.post('http://20.4.12.213:3000/user/login', {
    //             email: this.state.data.email,
    //             password: this.state.data.password
    //         }).then(response => {
    //             this.props.navigation.navigate('feed', this.state.data);
    //         }).catch(error => console.log(error));
    //     }
    // }
    UserLogin(){
        this.props.loginUser({
            email: this.state.data.email,
            password: this.state.data.password
        }).then(response => {
            console.log(response)
            this.props.navigation.navigate('feed',response)
        })
    }

    render(){
        
        
        return (
            <View style={style.container}>
                <View>
                    <Image
                        source={require('../images/logo.png')}
                    />
                    {/* <Text style={style.headerText}>Login</Text> */}
                </View>
                <View style={style.inputContainer}>
                <Image 
                        style={style.inputIcon}
                        source={require('../images/email.png')} />
                    <TextInput
                        style={style.textInput}
                        keyboardType="email-address"
                        textContentType="emailAddress"
                        placeholder="Type your email here"
                        value={this.state.email}
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, email: text}})
                            }
                        onEndEditing={()=>this.validate()}
                    /> 
                </View>
                <View style={style.inputContainer}>
                <Image 
                        style={style.inputIcon}
                        source={require('../images/lock.png')} />
                    <TextInput
                        style={style.textInput}
                        secureTextEntry={true}
                        textContentType="password"
                        placeholder="Type Your Password Here"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, password: text}})
                            }
                        onEndEditing={()=>this.validate()}
                    />
                </View>
                  <View>
                <Text style={style.errormessage}>{this.getErrorMessages()}</Text>
                </View>
                <View>
                    <TouchableHighlight style={[style.buttonContainer, style.loginButton]}
                        disabled={this.state.ButtonStateHolder}
                        onPress={()=>this.UserLogin()}>
                        <Text style={style.loginText}>Login</Text>
                    </TouchableHighlight>
                    
                    <TouchableHighlight style={style.buttonContainer}>
                        <Text>Don't have an account yet?</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={style.buttonContainer} onPress={() => this.props.navigation.navigate('register', this.state)}>
                        <Text>Sign Up</Text>
                    </TouchableHighlight>
                </View>
              
            </View>
        )
    }

    getErrorMessages(separator = '\n') {
        const { errors } = this.state;
        if (!errors) {return [];}
        return Object.values(errors).map(it => it.join(separator)).join(separator);
      }
      getErrorsInField(field) {
        const { errors } = this.state;
        return errors && errors[field] || [];
      }
      isFieldInError(field) {
        const { errors } = this.state;
        return errors && !!errors[field];
      }
    

}

const mapStateToProps = state => {
    return{
        user: state.user
    }
}

export default connect(mapStateToProps,{loginUser})(login)
