import React, {Component} from "react";
import {View, Text, TouchableOpacity, Image, Button} from 'react-native';
import style from '../styles/stylesCSS';
import {getFeed} from '../action/index';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker'
import { ScrollView, FlatList } from "react-native-gesture-handler";


class feedPage extends Component {
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props);
        const {getParam} = this.props.navigation;
        console.log(getParam('email'))
        this.state = {
           data: {
               email: getParam('email'),
            // email: 'keziaaaaa@RealUniversity.com',
               imageSource: '',
               imageData: ''
           }
        }
    }

    
    getFeedList(){
      this.props.getFeed(
        this.state.data.email
        )
      }
      componentDidMount(){
        this.getFeedList() 
      }

      renderItem = item => {
        return(
          <View style={style.list}>
          <ScrollView>
          <TouchableOpacity
          >
              <Image source={{uri:`http://20.4.12.213:3000/imagesFolder/${item.email}${item.name}`}}
                style={style.images}
              />
      <Text style={style.caption} >{item.caption}</Text>
          </TouchableOpacity>
        </ScrollView>
        </View>
     )     
  }

      displayFeed(){
        if(this.props.user.feeds.length){
          return (
            <FlatList 
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item})=>this.renderItem(item)}
                data={this.props.user.feeds}
              />
          )
        }
        return (
          <View style={style.container}>
                <Text style={style.blankFeed}> No activity on your feed, add friends or photos</Text>
            </View>
            
        )
      }

    render(){
      console.log(this.props.user.feeds)
        return(
            <View>
              <View>
                <Button 
                title='Add Photos'
                    onPress={() => {
                        const options = {
                            title: 'Select ProfilePic',
                            skipBackup: true,
                              storageOptions: {
                              path: 'images',
                            },
                            maxHeight: 800,
                            maxWidth: 800
                          };
                          
                          ImagePicker.showImagePicker(options, (response) => {
                            if (response.didCancel) {
                              console.log('User cancelled image picker');
                            } else if (response.error) {
                              console.log('ImagePicker Error: ', response.error);
                            } else if (response.customButton) {
                              console.log('User tapped custom button: ', response.customButton);
                            } else {
                              const source = { uri: response.uri };
                              this.setState({
                                ...this.state,
                                data: {
                                  ...this.state.data,
                                  imageSource: source,
                                  imageData: response.data,
                              }});
                              this.props.navigation.navigate('createFeed', this.state.data)
                            }
                          });
                    }}/>
            </View>
            <View>
            <Button
              title="Go to edit profile"
              onPress = {() => this.props.navigation.navigate('editProfile',this.state.data)}
            />
            </View>
              <View>
              {this.displayFeed()}
              </View>
            
            
        </View>
        )
    }
}

const mapStateToProps = state => {
  return{
      user:state.user
  }
}

export default connect(mapStateToProps,{getFeed})(feedPage)