import React , {Component} from 'react';
import {View, Image, Text, TextInput, TouchableOpacity, Button} from 'react-native';
import {connect} from 'react-redux';
import {postFeed} from '../action/index';
import style from '../styles/stylesCSS';


class createFeed extends Component {
   static navigationOptions= {
        header:null
    }

    constructor(props){
      super(props);
      const {getParam} = this.props.navigation;
      this.state={
        data: {
          email:getParam('email'),
          imageData: getParam('imageData'),
          imageSource: getParam('imageSource'),
          caption:''
        }
      }
    }

    postNewFeed(){
      this.props.postFeed(
        this.state.data.email,{
          caption:this.state.data.caption,
          imageData: this.state.data.imageData,
        }
      ).then(response => {
        this.props.navigation.navigate('feed', response)
      })
    }
    
    render(){
        return(
            <View style={style.container}>
              <View>
              <Text style={style.headerText}>Set Up Your Feed!</Text>
              </View>
              <View>
                <Image style={{width:300, height:300, borderColor:'white', borderWidth:4, marginBottom: 30,}}source={this.state.data.imageSource ? this.state.data.imageSource : require('../images/profile.png')} />
              </View>
                <View style={style.inputContainer}>
                    <TextInput
                    style={style.TextInput}
                        placeholder= "Express your feelings here!"
                        multiline={true}
                        onChangeText={(text) => this.setState({
                          data: {
                              ...this.state.data, caption: text}})
                          }
                    />
                </View>
                <View>
                   <Button
                  title="Submit"
                  onPress={() => this.postNewFeed('feed', this.state)}
                />
                </View>
            </View>
        )
    }
}



const mapStateToProps = state => {
  return{
      user:state.user
  }
}

export default connect(mapStateToProps,{postFeed})(createFeed)
