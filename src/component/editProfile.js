import React, {Component} from 'react';
import {View, Button, TextInput, Text, TouchableOpacity, Image} from 'react-native';
import {connect} from 'react-redux';
import {updateUser} from '../action/index';
import style from '../styles/stylesCSS';
import ImagePicker from 'react-native-image-picker';

class editProfile extends Component {
    static navigationOptions={
        title: 'Edit Profile'
    }

    constructor(props){
        super(props)
        const {getParam} = this.props.navigation;
        this.state={
            data: {
                email:getParam('email'),
                firstName: '',
                lastName:'',
                uniqueHandle:'',
                imageSource:'',
                profilePic:''
            }
        }
    }
    editProfileUpdate(){
        this.props.updateUser(
            this.state.data.email,{
            firstName: this.state.data.firstName,
            lastName: this.state.data.lastName,
            uniqueHandle: this.state.data.uniqueHandle,
            imageSource: this.state.data.imageSource,
        }).then(response => {
            this.props.navigation.navigate('feed',response)
        })
    }
    render(){
        console.log(this.state.data.email)
        return(
            <View style={style.container}>
                <View>
                    <Text style={style.headerText}>Edit Profile</Text>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        const options = {
                            title: 'Select ProfilePic',
                            storageOptions: {
                              skipBackup: true,
                              path: 'images',
                            },
                            maxHeight: 800,
                            maxWidth: 800
                          };
                          
                          ImagePicker.showImagePicker(options, (response) => {
                            if (response.didCancel) {
                              console.log('User cancelled image picker');
                            } else if (response.error) {
                              console.log('ImagePicker Error: ', response.error);
                            } else if (response.customButton) {
                              console.log('User tapped custom button: ', response.customButton);
                            } else {
                              const source = { uri: response.uri };
                              this.setState({
                                ...this.state,
                                imageSource: source,
                                data: {
                                  ...this.state.data,
                                  imageSource: response.data,
                              }});
                            }
                          });
                    }}>
                    <Image style={style.circleImageView2} source= {this.state.imageSource? this.state.imageSource : require('../images/profile.png')} /> 
                </TouchableOpacity>
                <View style={style.inputContainer}>
                    <TextInput
                    style={style.textInput}
                        textContentType="name"
                        placeholder= "Type your first name here!"
                        onChangeText= {(text) => this.setState({
                            data: {
                                ...this.state.data, firstName: text}})
                            }
                        value={this.state.data.firstName}
                        autoCapitalize="words" />
                </View>
                <View style={style.inputContainer}>
                    <TextInput
                    style={style.textInput}
                        textContentType="familyName"
                        placeholder="Type your last name"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, lastName: text}})
                            }
                        value={this.state.data.lastName}
                        autoCapitalize="words" />
                </View>
                <View style={style.inputContainer}>
                    <TextInput
                    style={style.textInput}
                        textContentType="username"
                        placeholder="Type your handle"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, uniqueHandle: text}})
                            }
                        value={this.state.data.uniqueHandle}
                    />
                </View>
                <View>
                    <TouchableOpacity
                    style={[style.buttonContainer, style.loginButton]}
                        onPress = {() => this.editProfileUpdate()} 
                    >
                        <Text style={style.loginText}>Save</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }   
}

const mapStateToProps = state => {
    return{
        user:state.user
    }
}

export default connect(mapStateToProps,{updateUser})(editProfile)
