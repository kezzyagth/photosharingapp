export const constraintsLogin = {
    email: {
      presence: {
        allowEmpty: false,
        message: '^Please enter an email address',
      },
      format: {
        pattern: /^\w+([\.-]?\w+)*@\RealUniversity.com/,
        message: '^Please enter a valid RealUniversity email address',
      },
    },
    password: {
      presence: {
        allowEmpty: false,
        message: '^Please enter a password',
      },
    }
  };
  export default constraintsLogin;