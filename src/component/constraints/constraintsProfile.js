

export const constraintsProfile = {
    firstName: {
        presence: {
          allowEmpty: false,
          message: '^Please enter your first name'
        },
        format: {
          pattern: /^[A-Za-z]{1,20}\z/,
          message: '^Please enter a valid first name'
        }
      },
      lastName: {
        presence: {
          allowEmpty: false,
          message: '^Please enter your last name'
        },
        format: {
          pattern: /^[A-Za-z]{1,20}\z/,
          message: '^Please enter a valid last name'
        }
      },
      handle: {
        presence: {
          allowEmpty: false,
          message: '^Please enter your handle'
        },
        format: {
          pattern: /^[0-9A-Za-z]{1,20}\z/,
          message: '^Please enter a valid handle'
        }
      },
};

export default constraintsProfile;