export const constraints = {
  email: {
    presence: {
      allowEmpty: false,
      message: '^Please enter an email address',
    },
    format: {
      pattern: /^\w+([\.-]?\w+)*@\RealUniversity.com/,
      message: '^Please enter a valid RealUniversity email address',
    },
  },
  password: {
    presence: {
      allowEmpty: false,
      message: '^Please enter a password',
    },
    length: {
      minimum: 6,
      tooShort: '^Password has to be 6 characters minimum',
    },
  },
  cpassword: {
    equality: {
      attribute: 'password',
      message: '^Both passwords has to be equal',
    },
  },
};
export default constraints;