import React, {Component} from 'react';
import {View, Button, Text, TextInput, TouchableOpacity, Image} from 'react-native';
import style from '../styles/stylesCSS';
import {validate} from 'validate.js';
import constraintsProfile from './constraints/constraintsProfile';
import {updateUser} from '../action/index';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';

class profilePage extends Component {
    static navigationOptions={
        title: 'Profile'
    }
    
    constructor(props){
        super(props);
        const {getParam} = this.props.navigation;
        this.state={
            data: {
                email:getParam('email'),
                firstName:'',
                lastName: '',
                uniqueHandle: '',
                imageSource: '',
                profilePic:''
            },
            // ButtonStateHolder: true,
            profilePic:'null',
            errors: '',
        };
        // this.validate = this.validate.bind(this)
    }   

    validate() {
        const validationResult = validate(this.state.data, constraintsProfile);
        this.setState({ errors: validationResult });
        /* if (!this.state.errors){
            this.EnableButtonFunction();
        } else {
            this.DisableButtonFunction();
        } */
    }

    updateUserProfile(){
        this.props.updateUser(
            this.state.data.email,{
            firstName: this.state.data.firstName,
            lastName: this.state.data.lastName,
            uniqueHandle: this.state.data.uniqueHandle,
            imageSource: this.state.data.imageSource,
        }).then(response => {
            this.props.navigation.navigate('feed',response)
        })
    }

    render(){
        return (
            <View style={style.container}>
                <View>
                    <Text style={style.headerText}>Profile</Text>
                </View>
                <TouchableOpacity  
                    onPress={() => {
                        const options = {
                            title: 'Select ProfilePic',
                            storageOptions: {
                              skipBackup: true,
                              path: 'images',
                            },
                            maxHeight: 800,
                            maxWidth: 800
                          };
                          
                          ImagePicker.showImagePicker(options, (response) => {
                            if (response.didCancel) {
                              console.log('User cancelled image picker');
                            } else if (response.error) {
                              console.log('ImagePicker Error: ', response.error);
                            } else if (response.customButton) {
                              console.log('User tapped custom button: ', response.customButton);
                            } else {
                              const source = { uri: response.uri };
                              this.setState({
                                ...this.state,
                                imageSource: source,
                                data: {
                                  ...this.state.data,
                                  imageSource: response.data,
                              }});
                            }
                          });
                    }}>
                    <Image style={style.circleImageView2}source={this.state.imageSource ? this.state.imageSource : require('../images/profile.png')} />
                </TouchableOpacity>
                <View style ={style.inputContainer}>
                    <TextInput
                    style={style.textInput}
                        textContentType="name"
                        placeholder="Type your first name"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, firstName: text}})
                            }
                        value={this.state.data.firstName}
                        autoCapitalize="words"
                        // onEndEditing={()=>this.validate()}
                    />
                </View>
                <View 
                    style={style.inputContainer}>
                    <TextInput
                    style={style.textInput}
                        textContentType="familyName"
                        placeholder="Type your last name"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, lastName: text}})
                            }
                        value={this.state.data.lastName}
                        autoCapitalize="words"
                        // onEndEditing={()=>this.validate()}
                    />
                </View>
                <View
                style={style.inputContainer}>
                    <TextInput
                    style={style.textInput}
                        textContentType="username"
                        placeholder="Type your handle"
                        onChangeText={(text) => this.setState({
                            data: {
                                ...this.state.data, uniqueHandle: text}})
                            }
                        value={this.state.data.uniqueHandle}
                        // onEndEditing={()=>this.validate()}
                    />
                </View>
                <View>
                    <TouchableOpacity
                    style={[style.buttonContainer, style.loginButton]}
                    // disabled={this.state.ButtonStateHolder}
                    onPress = {() => this.updateUserProfile()}>
                    <Text style={style.loginText}>Save</Text>
                    </TouchableOpacity>
                </View>
            <View>
                {/* <Text style={style.errormessage}>{this.getErrorMessages()}</Text>            */}
            </View>  
            </View> 
        )
    }

getErrorMessages(separator = '\n') {
    const { errors } = this.state;
    if (!errors) {return [];}
    return Object.values(errors).map(it => it.join(separator)).join(separator);
    }
    getErrorsInField(field) {
    const { errors } = this.state;
    return errors && errors[field] || [];
    }
    isFieldInError(field) {
    const { errors } = this.state;
    return errors && !!errors[field];
    }

}

const mapStateToProps = state => {
    return{
        user:state.user
    }
}

export default connect(mapStateToProps,{updateUser})(profilePage)