import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import login from './login';
import register from './register';
import profile from './profilePage';
import feed from './feedPage';
import createFeed from './createFeed';
import editProfile from './editProfile';

const MainNavigator = createStackNavigator(
  {
    login: {screen: login},
    register: {screen: register},
    profile: {screen: profile},
    feed: {screen: feed},
    createFeed: {screen: createFeed},
    editProfile: {screen: editProfile}
  },
  {
    initialRouteName: 'login',
  },
);

const Container = createAppContainer(MainNavigator);

export default Container;
