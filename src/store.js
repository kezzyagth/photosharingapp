import {applyMiddleware,createStore,compose} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/index'

const middlewares=[thunk]

const store=createStore(
    rootReducer,
    compose(
        applyMiddleware(...middlewares)
    )
)

export default store;